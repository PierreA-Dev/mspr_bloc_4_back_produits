const express = require("express");
const { collection, addDoc, getDocs, getDoc, doc, updateDoc, deleteDoc } = require("firebase/firestore");
const { PubSub } = require('@google-cloud/pubsub');
const authenticateToken = require('../middleware/authenticateToken');
require('dotenv').config();


// Initialisation de Pub/Sub
const pubSubClient = new PubSub();
const TOPIC_NAME = "channel_produits";

const publishMessage = async (topicName, message) => {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(message));
    await pubSubClient.topic(topicName).publishMessage({ data: dataBuffer });
    console.log(`Message publié au topic ${topicName}`);
  } catch (error) {
    console.error("Erreur lors de la publication du message:", error.message);
  }
};

const createPostRoutes = (db) => {
  const router = express.Router();

  // Route pour créer un produit
  router.post("/produits", authenticateToken, async (req, res) => {
    try {
      const { reference, libelle, format, pays, intensite, aromes, conditionnement, prix } = req.body;

      if (!reference || !libelle || !format || !pays || !intensite || !aromes || !conditionnement || !prix) {
        return res.status(400).json({ error: "Tous les champs sont obligatoires." });
      }

      const docRef = await addDoc(collection(db, "produits"), { reference, libelle, format, pays, intensite, aromes, conditionnement, prix });

      // Publier un message Pub/Sub
      await publishMessage(TOPIC_NAME, {
        action: "CREATE",
        productId: docRef.id,
        data: { reference, libelle, format, pays, intensite, aromes, conditionnement, prix },
      });

      res.json({ message: "Produit créé avec succès!", id: docRef.id });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

// Route pour récupérer les données de tous les produits
router.get("/produits", async (req, res) => {
  try {
    const querySnapshot = await getDocs(collection(db, "produits"));
    const documents = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
    
    // Publier un message Pub/Sub
    await publishMessage(TOPIC_NAME, {
      action: "FETCH_ALL",
      timestamp: new Date().toISOString(),
      productCount: documents.length,
    });

    res.json(documents);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

  // Route pour récupérer les données d'un produit
  router.get("/produits/:id", async (req, res) => {
    try {
      const docId = req.params.id;
      const docRef = doc(db, "produits", docId);
      const docSnap = await getDoc(docRef);

      if (docSnap.exists()) {
        const data = docSnap.data();
        res.json(data);
      } else {
        res.status(404).json({ message: "Aucun produit trouvé!" });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

  // Route pour modifier un produit
  router.put("/produits/:id", authenticateToken, async (req, res) => {
    try {
      const docId = req.params.id;
      const { reference, libelle, format, pays, intensite, aromes, conditionnement, prix } = req.body;

      if (!reference || !libelle || !format || !pays || !intensite || !aromes || !conditionnement || !prix) {
        return res.status(400).json({ error: "Tous les champs sont obligatoires." });
      }

      const docRef = doc(db, "produits", docId);
      await updateDoc(docRef, { reference, libelle, format, pays, intensite, aromes, conditionnement, prix });

      // Publier un message Pub/Sub
      await publishMessage(TOPIC_NAME, {
        action: "UPDATE",
        productId: docId,
        data: { reference, libelle, format, pays, intensite, aromes, conditionnement, prix },
      });

      res.json({ message: "Produit mis à jour avec succès!" });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

  // Route pour supprimer un produit
  router.delete("/produits/:id", authenticateToken, async (req, res) => {
    try {
      const docId = req.params.id;
      const docRef = doc(db, "produits", docId);
      await deleteDoc(docRef);

      // Publier un message Pub/Sub
      await publishMessage(TOPIC_NAME, {
        action: "DELETE",
        productId: docId,
      });

      res.json({ message: "Produit supprimé avec succès!" });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

  return router;
};

module.exports = createPostRoutes;