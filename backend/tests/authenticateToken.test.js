const authenticateToken = require('../middleware/authenticateToken');
const admin = require('../../config/firebaseAdmin'); // Assurez-vous que le chemin est correct
const httpMocks = require('node-mocks-http');

// Mock de `firebaseAdmin`
jest.mock('../../config/firebaseAdmin', () => ({
  auth: jest.fn().mockReturnThis(),
  verifyIdToken: jest.fn(),
}));

describe('authenticateToken middleware', () => {
  let req, res, next;

  beforeEach(() => {
    req = httpMocks.createRequest({
      headers: {
        authorization: 'Bearer test-token',
      },
    });
    res = httpMocks.createResponse();
    next = jest.fn();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should call next() if the token is valid', async () => {
    const mockDecodedToken = { uid: '12345' }; // Simuler un token décodé
    admin.verifyIdToken.mockResolvedValue(mockDecodedToken);

    await authenticateToken(req, res, next);

    expect(admin.verifyIdToken).toHaveBeenCalledWith('test-token');
    expect(req.user).toEqual(mockDecodedToken);
    expect(next).toHaveBeenCalled();
    expect(res.statusCode).toBe(200); // Aucun status envoyé ici, le middleware continue avec `next`
  });

  it('should return 401 if no token is provided', async () => {
    req.headers.authorization = undefined;
  
    await authenticateToken(req, res, next);
  
    expect(admin.verifyIdToken).not.toHaveBeenCalled();
    expect(res.statusCode).toBe(401);
    expect(res._getData()).toBe('Unauthorized'); // Correspond au message renvoyé par `res.sendStatus(401)`
    expect(next).not.toHaveBeenCalled();
  });
  
  it('should return 403 if the token is invalid', async () => {
    admin.verifyIdToken.mockRejectedValue(new Error('Invalid token'));
  
    await authenticateToken(req, res, next);
  
    expect(admin.verifyIdToken).toHaveBeenCalledWith('test-token');
    expect(res.statusCode).toBe(403);
    expect(res._getData()).toBe('Forbidden'); // Correspond au message renvoyé par `res.sendStatus(403)`
    expect(next).not.toHaveBeenCalled();
  });  
});