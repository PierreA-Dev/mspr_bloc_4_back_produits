const admin = require('../../config/firebaseAdmin');

const authenticateToken = async (req, res, next) => {
  const idToken = req.headers.authorization && req.headers.authorization.split(' ')[1];

  if (idToken == null) {
    return res.sendStatus(401); // Unauthorized
  }

  try {
    const decodedToken = await admin.auth().verifyIdToken(idToken);
    req.user = decodedToken;
    next();
  } catch (error) {
    res.sendStatus(403); // Forbidden
  }
};

module.exports = authenticateToken;