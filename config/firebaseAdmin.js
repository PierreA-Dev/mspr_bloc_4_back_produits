const admin = require('firebase-admin');
const serviceAccount = require('./AccountKey.json'); // Chemin correct vers votre fichier JSON

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://mspr-bloc-4-5135a.firebaseio.com" // Remplacez par l'URL de votre base de données
});

module.exports = admin;