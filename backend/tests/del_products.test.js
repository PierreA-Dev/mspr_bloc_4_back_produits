const request = require("supertest");
const express = require("express");
const { deleteDoc, doc } = require("firebase/firestore");
const createPostRoutes = require("../routes/post.routes"); // Chemin vers votre fichier de routes


// Mock de Firestore
jest.mock("firebase/firestore", () => {
  return {
    deleteDoc: jest.fn(),
    doc: jest.fn(() => ({})), // Mock de la fonction doc
  };
});

// Mock de l'authentification
jest.mock("../middleware/authenticateToken", () => {
  return (req, res, next) => {
    req.user = { id: 'testUserId' }; // Simuler un utilisateur authentifié
    next();
  };
});

const app = express();
app.use(express.json());
app.use("/", createPostRoutes({})); // Passer une instance de Firestore fictive

describe("Tests API pour supprimer un produit", () => {
  beforeEach(() => {
    jest.clearAllMocks(); // Réinitialiser les mocks avant chaque test
  });

  it("devrait supprimer un produit avec succès", async () => {
    const productId = "1"; // ID du produit à supprimer

    // Simuler que la suppression du document réussit
    deleteDoc.mockResolvedValueOnce({});

    const response = await request(app)
      .delete(`/produits/${productId}`) // Envoi de la requête DELETE
      .set('Authorization', `Bearer fakeToken`); // Ajouter un en-tête d'autorisation

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({ message: "Produit supprimé avec succès!" });

    // Vérifier que la fonction deleteDoc a été appelée avec la bonne référence
    expect(deleteDoc).toHaveBeenCalledWith(doc({}, productId));
  });

  it("devrait retourner une erreur si le produit n'existe pas", async () => {
    const productId = "nonExistentId"; // ID du produit inexistant

    // Simuler un échec de suppression (produit n'existe pas)
    deleteDoc.mockRejectedValueOnce(new Error("Aucun produit trouvé!"));

    const response = await request(app)
      .delete(`/produits/${productId}`) // Envoi de la requête DELETE
      .set('Authorization', `Bearer fakeToken`); // Ajouter un en-tête d'autorisation

    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual({ error: "Aucun produit trouvé!" });
  });
});