const request = require("supertest");
const express = require("express");
const { addDoc, collection } = require("firebase/firestore");
const createPostRoutes = require("../routes/post.routes");


// Mock de Firestore
jest.mock("firebase/firestore", () => {
  return {
    addDoc: jest.fn(), // Mock de la fonction addDoc pour simuler l'ajout dans Firestore
    collection: jest.fn(() => ({})), // Mock de collection
  };
});

// Mock de l'authentification
jest.mock("../middleware/authenticateToken", () => {
  return (req, res, next) => {
    req.user = { id: "testUserId" }; // Simuler un utilisateur authentifié
    next();
  };
});

const app = express();
app.use(express.json());
app.use("/", createPostRoutes({}));

describe("Tests API pour créer un produit", () => {

  beforeEach(() => {
    jest.clearAllMocks(); // Réinitialiser les mocks avant chaque test
  });

  it("devrait créer un produit avec succès", async () => {
    const newProduct = {
      reference: "REF123",
      libelle: "Café Arabica",
      format: "500g",
      pays: "Brésil",
      intensite: "8",
      aromes: "Fruité",
      conditionnement: "Paquet",
      prix: 7.99,
    };

    addDoc.mockResolvedValueOnce({ id: "newProductId" }); // Simuler l'ajout réussi dans Firestore

    const response = await request(app)
      .post("/produits")
      .send(newProduct)
      .set("Authorization", `Bearer fakeToken`); // Simuler un token d'authentification

    expect(response.statusCode).toBe(200); // Vérifier que la réponse a un statut 200
    expect(response.body).toEqual({
      message: "Produit créé avec succès!",
      id: "newProductId",
    });

    expect(addDoc).toHaveBeenCalledWith(collection({}, "produits"), newProduct);
  });

  it("devrait renvoyer une erreur si les champs obligatoires sont manquants", async () => {
    const incompleteProduct = {
      reference: "REF123",
      libelle: "Café Arabica",
    };

    const response = await request(app)
      .post("/produits")
      .send(incompleteProduct)
      .set("Authorization", `Bearer fakeToken`); // Simuler un token d'authentification

    expect(response.statusCode).toBe(400); // Vérifier que la réponse a un statut 400
    expect(response.body).toEqual({
      error: "Tous les champs sont obligatoires.",
    });

    expect(addDoc).not.toHaveBeenCalled(); // Vérifier que la fonction addDoc n'est pas appelée si les données sont incomplètes
  });
});