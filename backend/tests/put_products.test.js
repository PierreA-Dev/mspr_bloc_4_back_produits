const request = require("supertest"); 
const express = require("express");
const postRoutes = require("../routes/post.routes");
const { getFirestore } = require("firebase/firestore");

// Mock Firebase Admin SDK 
jest.mock("firebase-admin", () => ({
  auth: () => ({
    verifyIdToken: jest.fn().mockResolvedValue({ uid: "test-uid", email: "test@example.com" }),
  }),
  credential: {
    cert: jest.fn(),
  },
  initializeApp: jest.fn(),
}));

// Mock Firestore
jest.mock("firebase/firestore", () => ({
  getFirestore: jest.fn(),
  collection: jest.fn(),
  addDoc: jest.fn(),
  getDocs: jest.fn().mockResolvedValue({
    docs: [],
  }),
  doc: jest.fn(),
  getDoc: jest.fn(),
  updateDoc: jest.fn(),
  deleteDoc: jest.fn(),
}));

// Mock du middleware authenticateToken
jest.mock("../middleware/authenticateToken", () => {
  return jest.fn((req, res, next) => {
    req.user = { uid: "test-uid", email: "test@example.com" }; // Simuler un utilisateur authentifié
    next(); // Passe au prochain middleware
  });
});

// Créer une instance de l'application pour tester
const app = express();
app.use(express.json());
app.use("/", postRoutes(getFirestore()));

// Tests pour l'API des produits
describe("Tests API des produits", () => {
  beforeEach(() => {
    jest.clearAllMocks(); // Nettoyer les mocks avant chaque test
  });

  it("devrait mettre à jour un produit avec succès", async () => {
    const productId = "1"; // ID du produit à mettre à jour
    const updatedProduct = {
      reference: "REF001",
      libelle: "Café Expresso",
      format: "250g",
      pays: "Brésil",
      intensite: "fort",
      aromes: "chocolat",
      conditionnement: "sachet",
      prix: 15.00
    };

    // Mock de Firestore pour mettre à jour un produit
    const updateDoc = require("firebase/firestore").updateDoc;
    const docRef = { id: productId }; // Simuler une référence de document

    // Simuler que la mise à jour du document réussit
    updateDoc.mockResolvedValueOnce({});

    const response = await request(app)
      .put(`/produits/${productId}`)
      .send(updatedProduct);

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({ message: "Produit mis à jour avec succès!" });
  });

  it("devrait renvoyer une erreur si les champs obligatoires sont manquants", async () => {
    const productId = "1"; // ID du produit à mettre à jour
    const updatedProduct = {
      // Ne pas inclure 'reference' pour provoquer une erreur
      libelle: "Café Expresso",
      format: "250g",
      pays: "Brésil",
      intensite: "fort",
      aromes: "chocolat",
      conditionnement: "sachet",
      prix: 15.00
    };

    const response = await request(app)
      .put(`/produits/${productId}`)
      .send(updatedProduct);

    expect(response.statusCode).toBe(400);
    expect(response.body).toEqual({ error: "Tous les champs sont obligatoires." });
  });
});