const request = require("supertest");
const express = require("express");
const { getDoc, doc } = require("firebase/firestore");
const createPostRoutes = require("../routes/post.routes"); // Chemin vers votre fichier de routes

// Mock de Firestore 
jest.mock("firebase/firestore", () => {
  return {
    getDoc: jest.fn(),
    doc: jest.fn(() => ({})), // Mock de la fonction doc
  };
});

// Mock de l'authentification
jest.mock("../middleware/authenticateToken", () => {
  return (req, res, next) => {
    req.user = { id: 'testUserId' }; // Simuler un utilisateur authentifié
    next();
  };
});

const app = express();
app.use(express.json());
app.use("/", createPostRoutes({})); // Passer une instance de Firestore fictive

describe("Tests API pour récupérer les données d'un produit", () => {
  beforeEach(() => {
    jest.clearAllMocks(); // Réinitialiser les mocks avant chaque test
  });

  it("devrait récupérer un produit depuis Firestore si non en cache", async () => {
    const productId = "1"; // ID du produit à récupérer
    const firestoreProduct = {
      reference: "REF123",
      libelle: "Café Arabica",
      format: "500g",
      pays: "Brésil",
      intensite: "8",
      aromes: "Fruité",
      conditionnement: "Paquet",
      prix: 7.99,
    };

    // Simuler la réponse Firestore pour getDoc
    getDoc.mockResolvedValueOnce({
      exists: () => true,
      data: () => firestoreProduct,
    });

    const response = await request(app)
      .get(`/produits/${productId}`) // Envoi de la requête GET
      .set('Authorization', `Bearer fakeToken`); // Ajouter un en-tête d'autorisation

    expect(response.statusCode).toBe(200); // Vérifiez que le statut est 200
    expect(response.body).toEqual(firestoreProduct); // Vérifiez que les données sont correctes
    expect(getDoc).toHaveBeenCalledWith(doc({}, productId)); // Vérifiez que Firestore a été appelé avec le bon ID
  });

  it("devrait retourner une erreur si le produit n'existe pas dans Firestore", async () => {
    const productId = "nonExistentId"; // ID d'un produit qui n'existe pas

    // Simuler que le produit n'existe pas dans Firestore
    getDoc.mockResolvedValueOnce({
      exists: () => false,
    });

    const response = await request(app)
      .get(`/produits/${productId}`) // Envoi de la requête GET
      .set('Authorization', `Bearer fakeToken`); // Ajouter un en-tête d'autorisation

    expect(response.statusCode).toBe(404); // Vérifiez que la réponse est 404
    expect(response.body).toEqual({ message: "Aucun produit trouvé!" }); // Vérifiez le message d'erreur
  });

  it("devrait retourner une erreur serveur en cas de problème Firestore", async () => {
    const productId = "1"; // ID du produit

    // Simuler une erreur dans Firestore
    getDoc.mockRejectedValueOnce(new Error("Erreur interne de Firestore"));

    const response = await request(app)
      .get(`/produits/${productId}`) // Envoi de la requête GET
      .set('Authorization', `Bearer fakeToken`); // Ajouter un en-tête d'autorisation

    expect(response.statusCode).toBe(500); // Vérifiez que le statut est 500
    expect(response.body).toEqual({ error: "Erreur interne de Firestore" }); // Vérifiez le message d'erreur
  });
});