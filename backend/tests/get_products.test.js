jest.mock("firebase-admin", () => ({
    auth: () => ({
      verifyIdToken: jest.fn().mockResolvedValue({ uid: "test-uid", email: "test@example.com" }),
    }),
    credential: {
      cert: jest.fn(),
    },
    initializeApp: jest.fn(),
  }));
   
  jest.mock("firebase/firestore", () => ({
    getFirestore: jest.fn(),
    collection: jest.fn(),
    getDocs: jest.fn().mockResolvedValue({
      docs: [
        { id: "1", data: () => ({ reference: "REF001", libelle: "Café", format: "250g", pays: "Brésil", prix: 12.50 }) },
        { id: "2", data: () => ({ reference: "REF002", libelle: "Thé", format: "100g", pays: "Inde", prix: 8.50 }) }
      ]
    }),
    doc: jest.fn(),
    getDoc: jest.fn(),
    updateDoc: jest.fn(),
    deleteDoc: jest.fn(),
  }));
  
  const request = require("supertest");
  const express = require("express");
  const postRoutes = require("../routes/post.routes");
  const { getFirestore } = require("firebase/firestore");
  
  // Créer une instance de l'application pour tester
  const app = express();
  app.use(express.json());
  app.use("/", postRoutes(getFirestore()));
  
  describe("Tests API des produits", () => {
    it("devrait récupérer tous les produits avec succès", async () => {
      const response = await request(app).get("/produits");
  
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveLength(2);  // Nous attendons 2 produits
      expect(response.body[0]).toHaveProperty("id", "1");
      expect(response.body[0]).toHaveProperty("libelle", "Café");
      expect(response.body[1]).toHaveProperty("id", "2");
      expect(response.body[1]).toHaveProperty("libelle", "Thé");
    });
  });  